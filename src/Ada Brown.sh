#!/bin/bash
. var
name="Ada Brown"

cd "$dir"
mkdir -p "$name"
cd "$name"

run="youtube-dl --audio-format flac -x"

# Evil Mama Blues
$run https://youtu.be/I_7dNSVpKlI

# Break O' Day Blues
$run https://youtu.be/dSDZ1enF1-k

# Panama Limited Blues
$run https://youtu.be/dwdkGPyrw_w

# Fats Waller & Ada Brown - That Ain't Right
$run https://youtu.be/C4ZFsm9msv4
