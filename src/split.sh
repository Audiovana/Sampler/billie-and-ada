#!/bin/bash
. var
out="$dir/output"

Billie(){
  spleeter separate -i $dir/Billie\ Holiday/* -p spleeter:2stems -o $out/Billie\ Holiday
}

Ada(){
  spleeter separate -i $dir/Ada\ Brown/* -p spleeter:2stems -o $out/Ada\ Brown
}

main(){
  mkdir -p $dir/.ai
  cd $dir/.ai
  Billie
  Ada
}
main
